import { buildSchema, convertSchemas } from ".";

const baseSchema = `
"""
type description
"""
type Query {
  "field description"
  testBaseQuery: String
}

"this is a mutation"
type Mutation {
  testMutation: String
}
`;

const extendedSchema = `
extend type Query {
  """
  description on extended type
  """
  testExtendQuery: String
}

type Subscription {
  testUpdate: String
  @aws_subscribe(mutations: ["testMutation"])
}
`;

const schemaWithDirectives = `
extend type Query
{
  """
  This field is deprecated.
  """
  foo: String
    @aws_api_key
    @aws_iam
    @aws_oidc
    @aws_cognito_user_pools
    @aws_auth(cognito_groups: ["users"])
    @deprecated(reason: "test")
}
`;

it("must not leak artifacts of default scalars and directives.", async () => {
  const schema = await convertSchemas("");

  expect(schema).toBe("");
});

it("should convert literal descriptions into comment descriptions.", async () => {
  const schema = await convertSchemas(baseSchema, { commentDescriptions: true });

  expect(/# type description/.test(schema) && /# field description/.test(schema)).toBe(true);
});

it("should merge type extensions.", async () => {
  const schema = await convertSchemas([baseSchema, extendedSchema], { commentDescriptions: true });

  expect(schema).toMatchSnapshot();
});

it("should retains AppSync scalars.", async () => {
  const emptySchema = await buildSchema("");
  const scalars = Object.keys(emptySchema.getTypeMap()).filter((__typename) => /^AWS/.test(__typename));
  const scalarsSDL = scalars.map((scalar) => `${scalar.replace(/^AWS/, "").toLowerCase()}: ${scalar}`);
  const schema = await convertSchemas(
    [
      baseSchema,
      `
      extend type Query {
        ${scalarsSDL.join("\n")}
      }
      `,
    ],
    { commentDescriptions: true },
  );

  expect(schema).toMatchSnapshot();
});

it("should retains AppSync directives.", async () => {
  const schema = await convertSchemas([baseSchema, schemaWithDirectives], {
    commentDescriptions: true,
    includeDirectives: true,
  });

  expect(schema).toMatchSnapshot();
});
