import {
  buildASTSchema,
  extendSchema,
  isScalarType,
  isSpecifiedDirective,
  parse,
  DefinitionNode,
  GraphQLDirective,
  GraphQLType,
} from "graphql";
import { printSchema, isDefinedType } from "./schemaPrinter";
import AppSyncScalars from "./scalars.graphql";
import AppSyncDirectives from "./directives.graphql";

/**
 * Do not print @aws_* directives
 */
const directiveFilter = (directive: GraphQLDirective) =>
  !isSpecifiedDirective(directive) && /^aws$/.test(directive.name);

/**
 * Do not print AWS* scalars
 */
const typeFilter = (type: GraphQLType) => isDefinedType(type) && !(isScalarType(type) && /^AWS/.test(type.name));

/**
 * Convert Apollo GraphQL schemas into AppSync verison.
 *
 * @param {[string]} An array of GraphQL SDL string to be converted.
 * @param {object} options
 *   - {boolean} options.commentDescriptions Use legacy comment as type description.
 *   - {boolean} options.includeDirectives Include field directive declarations as output.
 */
export const convertSchemas = (
  schemas: string | string[],
  { commentDescriptions = false, includeDirectives = false } = {},
) =>
  Promise.resolve(schemas)
    .then((schemas) => (Array.isArray(schemas) ? schemas.join("\n") : schemas))
    .then(buildSchema)
    .then((schema) => printSchema(schema, { commentDescriptions, directiveFilter, includeDirectives, typeFilter }))
    .then((schema) => schema.replace(AppSyncScalars, "").replace(AppSyncDirectives, "").trim());

export const buildSchema = (schemas: string) =>
  Promise.resolve(schemas)
    .then((schemas) => [AppSyncScalars, AppSyncDirectives].concat(schemas).join("\n"))
    .then((schemas) => parse(schemas))
    .then((schema) =>
      // buildSchema() does not handles type extension, extendSchema() is required.
      schema.definitions.reduce<[DefinitionNode[], DefinitionNode[]]>(
        ([defs, exts], astNode) => {
          if (/Extension$/.test(astNode.kind)) {
            exts.push(astNode);
          } else {
            defs.push(astNode);
          }

          return [defs, exts];
        },
        [[], []],
      ),
    )
    .then(([defs, exts]) =>
      extendSchema(buildASTSchema({ kind: "Document", definitions: defs }), {
        kind: "Document",
        definitions: exts,
      }),
    );

export { printSchema, printIntrospectionSchema, printType } from "./schemaPrinter";
export { default as AppSyncScalars } from "./scalars.graphql";
export { default as AppSyncDirectives } from "./directives.graphql";
