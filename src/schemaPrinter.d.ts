import {
  GraphQLEnumType,
  GraphQLInputObjectType,
  GraphQLInterfaceType,
  GraphQLObjectType,
  GraphQLScalarType,
  GraphQLSchema,
  GraphQLType,
  GraphQLUnionType,
} from "graphql";

export type { printIntrospectionSchema } from "graphql";

export type PrintOptions = {
  commentDescriptions: Boolean;
  includeDirectives: Boolean;
  directiveFilter?: (directive: any) => Boolean;
  typeFilter?: (type: any) => Boolean;
};

export function printSchema(schema: GraphQLSchema, options: PrintOptions): string;

export function printType(
  type:
    | GraphQLEnumType
    | GraphQLInputObjectType
    | GraphQLInterfaceType
    | GraphQLObjectType
    | GraphQLScalarType
    | GraphQLUnionType,
  options: PrintOptions,
): string;

export function isDefinedType(type: GraphQLType): boolean;
